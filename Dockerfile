FROM localhost:1234/registry/gitlab-ci-cd/sailsjs:0.12.14
LABEL Author="victor" 
USER root
WORKDIR /var/www
COPY ./ /var/www
RUN cd /var/www && rm -rf node_modules
RUN npm install
RUN rm package-lock.json
EXPOSE 443
ENV NODE_ENV production
ENV TZ=America/Sao_Paulo
CMD ["npm", "start"]
