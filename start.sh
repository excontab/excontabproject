#!/bin/bash
echo "Parando Imagens......................................................"
docker-compose down
echo "Atualizando imagens.................................................."
docker-compose pull
if [ "$?" != "0" ]; then
    echo "Falha ao atualizar as images!"
    exit 999
fi

echo "Inicializando containers............................................."
docker-compose up -d
if [ "$?" != "0" ]; then
    echo "Falha ao inicializar as images!"
    exit 999
fi

echo "Containers Inicializados!............................................"
docker-compose ps
echo "Removendo imagens antigas............................................"
docker image prune -a -f