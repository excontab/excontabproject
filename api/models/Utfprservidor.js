module.exports = {
    attributes: {
      nome: { type: 'string' },
      telefone: { type: 'integer' },
      ramal: { type: 'integer' },
      sala: { type: 'string' },
      cargo: { type: 'string' },
      matricula: { type: 'integer' },
      celular: { type: 'integer' },
      email: { type: 'string' }
    }
  };