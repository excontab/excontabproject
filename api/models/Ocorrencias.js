module.exports = {
    attributes: {
      id_veiculo: { type: 'integer' },
      id_estacionamento: { type: 'integer' },
      observacao: { type: 'string' }
    }
  };
