module.exports = {
  attributes: {
    nome: { type: 'string' },
    cpfcnpj: { type: 'string' },
    rua: { type: 'string' },
    numero: { type: 'integer' },
    bairro: { type: 'string' },
    cidade: { type: 'string' },
    pais: { type: 'string' },
    complemento: { type: 'string' },
    banco: { type: 'string' },
    agencia: { type: 'integer' },
    conta: { type: 'integer' },
    operacao: { type: 'integer' },
    vhora: { type: 'float' }
  }
};