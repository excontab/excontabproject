const bcrypt = require('bcryptjs');

module.exports = {
  attributes: {
    email:{type: 'string',required: true, unique: true},
    nome:{type: 'string', required: true},
    password:{ type: 'string', required: true },
    ativo: { type: 'boolean', defaultsTo: true },
    is_admin: { type: 'boolean', defaultsTo: false },
  },
  beforeCreate: function (user, cb) {
    if (user.password) {
      bcrypt.genSalt(10, function (err, salt) {
        bcrypt.hash(user.password, salt, function (err, hash) {
          if (err) {
            console.log(err);
            cb(err)
          } else {
            user.password = hash;
            cb();
          }
        });
      });
    } else {
      cb();
    }
  },
  beforeUpdate: function (user, cb) {
    if (user.password) {
      bcrypt.genSalt(10, function (err, salt) {
        bcrypt.hash(user.password, salt, function (err, hash) {
          if (err) {
            console.log(err);
            cb(err)
          } else {
            user.password = hash;
            cb();
          }
        });
      });
    } else {
      cb();
    }
  }
};

