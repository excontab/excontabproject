module.exports = {
    attributes: {
      nome: { type: 'string' },
      cpf: { type: 'string' },
      celular: { type: 'integer' },
      email: { type: 'string' }
    }
  };