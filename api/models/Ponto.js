module.exports = {
    attributes: {
        id_func: {type: 'integer'},
        totalhoras: { type: 'float' },
        hora_inicio: { type: 'float' },
        hora_inicioalmoco: { type: 'float' },
        hora_fimalmoco: { type: 'float' },
        hora_fim: { type: 'float' },
    }
  };