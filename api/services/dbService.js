const { Client } = require('pg');

module.exports = {
    get: (sql, callback) =>{
        let callCB = false;
        try {
          const client = new Client(sails.config.db_conf);
          client.connect((err) => {
            if (err){
              callCB = true;
              return callback({status: -1, msg: "Falha ao conectar ao banco de dados!"});
            } 
          });
      
          client.query(sql, (err, res) => {
            if(err){
              client.end();
              console.log(JSON.stringify(err))
              if (!callCB) return callback({status: -1, msg: "Falha ao buscar os dados do banco!"});
            }else{
              if (res.rows && !callCB){
                  client.end();
                  return callback({status: 1, result: res.rows});
              }else{
                 client.end();
                 if (!callCB) return callback({status: -1, result: res});
              }
            }
          });
      
        } catch (e) {
            return callback({status: -1, msg: "Falha ao conectar ao banco de dados!"});
        }
    }      
}