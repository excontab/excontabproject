
module.exports = {
    passaMinutos: (hora) => {
        var h = parseInt(hora);
        var m = (hora - h) * 100;
        return ((h * 60) + m);
    },
};