const moment = require('moment');
const bluebird = require('bluebird');
const VeiculoController = require('./VeiculoController');

module.exports = {
    new: (req, res) => {
        Veiculo.find().exec((err, vec) => {
            Utfprestacionamento.find().exec((err, est) => {
                return res.view({veiculo: vec, estacionamento: est});
            });
        });
    },
    index: (req, res) => {
        Veiculo.find().exec((err, vec) => {
            return res.view({veiculo: vec});
        });
    },
    find: (req, res) => {
        var idVeiculo = req.param("veiculo");
        Ocorrencias.find({id_veiculo: idVeiculo}).exec((err, oco) => {
            return res.view({ocorrencias: oco});
        });
    },
    create: (req, res) => {
        var o = {
            id_veiculo: req.param('veiculo'),
            id_estacionamento: req.param('estacionamento'),
            observacao: req.param('observacao'),
        };
        Ocorrencias.create(o, (err, oco) => {

            if (err) {
                flashService.createSessionFlash(req, `Erro ao criar a Ocorrencia, info:`, 'error');
                return res.redirect('/funcionario/new');
            } else {
                flashService.createFlashPadrao(req, 1);
                return res.redirect('/funcionario');
            }
        });
    },
};

