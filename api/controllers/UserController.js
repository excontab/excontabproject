const bcrypt = require('bcryptjs');

module.exports = {
    index: (req, res) => {
        User.find().exec((err, users) => {
            if (err || !users) {
                flashService.createSessionFlash(req, `Falha ao buscar os usuários, info:  ${err}`, 'error');
                return res.view({ users: [] });
            } else {
                return res.view({ users: users });
            }
        });
    },
    update: (req, res) => {
        let id = req.param('id');
        let linkEdit = (id) ? `/user/edit/${id}` : '/user/edit/';

        let p = {
            email: req.param('email').trim(),
            password: req.param('password').trim(),
            ativo: (req.param('ativo') == "on"),
            is_admin: (req.param('is_admin') == "on"),
        };

        if (!p.password || !p.email) {
            flashService.createSessionFlash(req, `Favor informe os dados corretamente!`, 'warning');
            return res.redirect(linkEdit);
        }
        if (!id) {
            User.create(p, (err, user) => {
                if (err) {
                    flashService.createSessionFlash(req, `Erro ao criar o usuário, info: ${err}`, 'error');
                } else {
                    flashService.createSessionFlash(req, `Usuário ${p.email} criado com sucesso!`, 'success');
                }
                return res.redirect('/user');
            });
        }else{
            User.update(id, p, (err, user) => {
                if (err) {
                    flashService.createSessionFlash(req, `Erro ao atualizar o usuário, info: ${err}`, 'error');
                }else{
                    flashService.createSessionFlash(req, `Usuário ${p.email} atualizado com sucesso!`, 'success');
                }
                return res.redirect('/user');
            });
        }
    },
    edit: (req, res) => {
        let id = req.param('id');
        if (!id){
            return res.view({ user: null });
        } else{
            User.findOne(id, (errFO, pFO) => {
                if (errFO || !pFO) return res.view({ user: null });
                return res.view({ user: pFO });
            });
        }

    },
    destroy: (req, res) => {
        flashService.createFlashPadrao(req, 8);
        return res.redirect('/user');
    },
    login:(req,res) => {
        return res.view({ layout: '' });
    },

    logar: (req, res) => {
        if (sails.config.environment != "production") {
            let randomImage = Math.floor(Math.random() * 10 + 1);
            User.find().where({ email: 'admin'}).exec(function userFounded(err, user) {
                if (user && Object.keys(user).length > 0) {
                    req.session.authenticated = true;
                    req.user= user[0];
                    req.user.image = "/images/users/"+randomImage+".jpg";
                    req.user.nome = "Bem vindo!";
                    req.session.usuario = user[0];
                    req.session.usuario.image = "/images/users/"+randomImage+".jpg";
                    req.session.usuario.nome = "Bem vindo!";
                    return res.redirect('/');
                }else{
                    return res.redirect('/user/login');
                }
            });
        }else{
            return res.redirect('/user/login');
        }
    },
    logout: (req, res) => {
        req.session.authenticated = false;
        req.user = null;
        req.session.usuario = null;
        return res.redirect("/user/login");
    },
    dados: (req,res) => {
        if (!req.session.usuario || !req.session.usuario.id){
            return res.redirect('/');
        } else{
            User.findOne(req.session.usuario.id, (errFO, pFO) => {
                if (errFO || !pFO) return res.redirect('/');
                return res.view({ user: pFO });
            });
        }
    },
    updateDados: (req,res) => {
        let p = {
            page: req.param('page'),
        };
        console.log("here");
        if (!p.page) {
            flashService.createSessionFlash(req, `Favor informe os dados corretamente!`, 'warning');
            return res.redirect("/user/dados");
        }
        console.log("here2");
        if (!req.session.usuario || !req.session.usuario.id) {
            console.log("here4");
            return res.redirect("/")
        }else{
            console.log("here3");
            console.log(req.session.usuario.id)
            User.update(req.session.usuario.id, p, (err, user) => {
                if (err) {
                    console.log("here5");
                    flashService.createSessionFlash(req, `Erro ao atualizar o usuário, info: ${err}`, 'error');
                }else{
                    flashService.createSessionFlash(req, `Usuário atualizado com sucesso!`, 'success');
                    req.session.usuario.page = p.page
                }
                return res.redirect('/');
            });
        }
    }
};