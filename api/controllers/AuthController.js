module.exports = {
    logar: (req, res) => {
        let username = req.param('username');
        let pwd = req.param("password")
        User.find({ nome: username }).exec((Err, users) => {
            if (users && users.length > 0) {
                if (users[0].password == pwd) {
                    req.session.authenticated = true;
                    req.session.usuario = users[0];
                    console.log("authenticated")
                    return res.redirect('/');
                } else {
                    flashService.createSessionFlash(req, `dados inválidos`, 'error');
                    return res.redirect('/user/login');
                }
            } else {
                flashService.createSessionFlash(req, `dados inválidos`, 'error');
                return res.redirect('/user/login');
            }
        });
    },
    logout: function (req, res) {
        req.session.authenticated = false;
        req.session.usuario = null;
        req.logout();
        return res.redirect('/user/login');
    },
};