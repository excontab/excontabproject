const bluebird = require('bluebird');
const moment = require('moment');
module.exports = {
    index: (req,res) => {
        if (req.session.usuario && req.session.usuario.id){
            funcionario.find({user: req.session.usuario.id, status: ['to review']}).populateAll().sort("id DESC").exec((err, funcionarios) => {
                if(err || !funcionarios){
                    flashService.createSessionFlash(req, `Falha ao buscar as funcionarios, info:  ${err}`, 'error');
                    return res.view({funcionarios: [], moment: moment});
                }else{
                    return res.view({funcionarios: funcionarios, moment: moment});
                }
            });
        }else{
            return res.view({funcionarios: [], moment: moment});
        }
    },
    admin: (req,res) => {
        User.count({ativo: true}).exec( (err, users) => {
            Job.count().exec( (err, jobs) => { 
                funcionario.count().exec( (err, funcionarios) => {
                    return res.view({users:users,jobs:jobs,funcionarios:funcionarios});
                });
            });
        });
    }
};

