const moment = require('moment');
const bluebird = require('bluebird');
const util = require('util');
const { decodeBase64 } = require('bcryptjs');
const pontoService = require('../services/pontoService');

module.exports = {
    index: (req, res) => {
        Funcionario.find().sort("nome").exec((err, usuarios) => {
            return res.view({ funcionario: usuarios });
        });
    },
    create: (req, res) => {
        //se o valor da hora de fim for menor quealquer uma somar 1440
        // validar se uma hora nao é menor que a outra se for add 1440
        var th = pontoService.passaMinutos(req.param("jornada"));
        var hi = pontoService.passaMinutos(req.param("entrada"));
        var hia = pontoService.passaMinutos(req.param("ent_almoco"));
        var hfa = pontoService.passaMinutos(req.param("sai_almoco"));
        var hf = pontoService.passaMinutos(req.param("saida"));


        if (hia < hi) {
            hia += 1440;
        }
        if (hfa < hia) {
            hfa += 1440;
        }
        if (hf < hfa) {
            hf += 1440;
        }

        var p = {
            id_func: req.param('funcionarioS'),
            totalhoras: th,
            hora_inicio: hi,
            hora_inicioalmoco: hia,
            hora_fimalmoco: hfa,
            hora_fim: hf,
        };
        Ponto.create(p, (err, ponto) => {
            if (err) {
                flashService.createSessionFlash(req, `Erro ao criar a Funcionario, info:`, 'error');
                return res.redirect('/funcionario/new');
            } else {
                flashService.createFlashPadrao(req, 1);
                return res.redirect('/funcionario');
            }
        });
    }
}