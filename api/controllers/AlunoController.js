const moment = require('moment');
const bluebird = require('bluebird');
const Utfprservidor = require('../models/Utfprservidor');

module.exports = {
    new: (req, res) => {
        return res.view({});
    },
    edit: (req, res) => {
        let id = req.param('id');
        let isEdit = req.param("is_edit");
            
        Utfpraluno.find(id).exec((err, alu) => {
            if (!isEdit) {
                return res.view({ aluno: alu, editavel: false });
            } else {
                return res.view({ aluno: alu, editavel: true });
            }
        });
    },
    index: (req, res) => {
        Utfpraluno.find().sort("nome").exec((err, alu) => {
            if (!err) {
                return res.view({ aluno: alu });
            } else {
                return res.view({ aluno: null });
            }
        });
    },
    create: (req, res) => {
        let id = req.param("id");
        let a = {
            nome: req.param("nome"),
            curso: req.param("curso"),
            ra: req.param("ra"),
            celular: req.param("celular"),
            email: req.param("email")
        };
        if (id) {
            Utfpraluno.update(id, a, (err, alu) => {
                if (err) {
                    flashService.createSessionFlash(req, `Erro ao atualizar Aluno da UTFPR, info:`, 'error');
                    return res.redirect('/aluno/edit');
                } else {
                    flashService.createFlashPadrao(req, 1);
                    return res.redirect('/aluno');
                }
            });
        } else {
            Utfpraluno.create(a, (err, alu) => {
                if (err) {
                    flashService.createSessionFlash(req, `Erro ao cadastrar Aluno da UTFPR, info:`, 'error');
                    return res.redirect('/aluno/new');
                } else {
                    flashService.createFlashPadrao(req, 1);
                    return res.redirect('/aluno');
                }
            });
        }
    },
    destroy: (req, res) => {
        Utfprsaluno.destroy(id, (err, serv) => {
            flashService.createFlashPadrao(req, 8);
            return res.redirect('/aluno');
        });
    }
}

