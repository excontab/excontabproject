const moment = require('moment');
const bluebird = require('bluebird');

module.exports = {
    new: (req, res) => {
        return res.view({});
    },
    index: (req, res) => {
        Estacionamento.find().exec((err, estacionamento) => {
            return res.view({ estacionamento: estacionamento });
        });
    },
    create: (req, res) => {
        var e = {
            descricao: req.param('descricao'),
            codigo: req.param('codigo')
        };
        Estacionamento.create(f, (err,) => {

            if (err) {
                flashService.createSessionFlash(req, `Erro ao criar o estacionamento, info:`, 'error');
                return res.redirect('/estacionamento/new');
            } else {
                flashService.createFlashPadrao(req, 1);
                return res.redirect('/estacionamento');
            }
        });
    },
};

