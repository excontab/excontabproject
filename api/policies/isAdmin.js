module.exports = function (req, res, ok) {
    if (req.session.authenticated && req.session.usuario) {
        if (req.session.usuario.is_admin == true) return ok();
        return res.redirect("/")
    }
}
