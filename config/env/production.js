module.exports = {
  models: {
    connection: 'Excontab'
  },
  port: 80,
  session: {
    adapter: 'connect-redis',
    host: 'localhost',
    port: 6379,
    ttl: 86400,
    db: 0,
    pass: "postgres",
    prefix: 'excontab-sess:',
    key: 'excontab'
  },
  db_conf: {
    host: 'postgres-prod',
    user: 'postgres',
    password: 'postgres',
    database: 'excontab-producao',
    port: 5432
  }
};
