module.exports = {
  port: 80,
  models: {
    connection: 'dbDev'
  },
  db_conf: {
    host: 'localhost',
    user: 'octopus',
    password: 'octopus_web',
    database: 'excontab-dev',
    port: 5432
  }
};
