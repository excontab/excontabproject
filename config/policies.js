module.exports.policies = {
  '*': ['flash','isAuthenticated'],
  'user': {
    'login': ['flash'],
    'logar': ['flash'],
    'logout': ['flash'],
    'index':['flash','isAuthenticated','isAdmin'],
    'update':['flash','isAuthenticated','isAdmin'],
    'edit':['flash','isAuthenticated','isAdmin'],
    'destroy':['flash','isAuthenticated','isAdmin'],
    'dados': ['flash','isAuthenticated'],
    'updateDados': ['flash','isAuthenticated']
  },
  'auth': ['flash'],
  'job':{
    '*': ['flash','isAuthenticated'],
    'gettoreview': ['flash'],
    'updateReport': ['flash']
  },
  'dashboard':{
    '*': ['flash','isAuthenticated'],
    'admin': ['flash','isAuthenticated','isAdmin']
  }
};
