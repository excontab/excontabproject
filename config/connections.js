module.exports.connections = {
  dbDev: {
    adapter: 'sails-postgresql',
    host: 'localhost',
    user: 'postgres',
    password: 'postgres',
    database: 'controlecarros',
    port:5433
  },
  excontabProd:{
    adapter: 'sails-postgresql',
    host: 'localhost',
    user: 'postgres',
    password: 'postgres',
    database: 'controlecarros',
  },
  local:{
    adapter: 'sails-postgresql',
    host: 'localhost',
    user: 'postgres',
    password: 'postgres',
    database: 'controlecarros',
  }
};


