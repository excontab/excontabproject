CREATE SEQUENCE public.placas_pessoas_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;
CREATE TABLE public."placas_pessoas"
(
    id_carro integer NOT NULL,
	placa_carro text NOT NULL COLLATE pg_catalog."default",
	id_servidor_aluno_visitante integer NOT NULL,
	tipo_pessoa char NOT NULL,
    id integer NOT NULL DEFAULT nextval('placas_pessoas_id_seq'::regclass),
    "createdAt" timestamp with time zone,
    "updatedAt" timestamp with time zone,
    CONSTRAINT placas_pessoas_pkey PRIMARY KEY (id)
)