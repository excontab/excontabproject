CREATE SEQUENCE public.funcionario_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

CREATE TABLE public.funcionario
(
    nome text COLLATE pg_catalog."default",
    rua text COLLATE pg_catalog."default",
    numero integer,
    bairro text COLLATE pg_catalog."default",
    cidade text COLLATE pg_catalog."default",
    id integer NOT NULL DEFAULT nextval('funcionario_id_seq'::regclass),
    "createdAt" timestamp with time zone,
    "updatedAt" timestamp with time zone,
    pais text COLLATE pg_catalog."default",
    complemento text COLLATE pg_catalog."default",
    cpfcnpj text COLLATE pg_catalog."default",
    banco text COLLATE pg_catalog."default",
    agencia integer,
    conta integer,
    operacao integer,
    vhora real,
    CONSTRAINT funcionario_pkey PRIMARY KEY (id)
)
