CREATE SEQUENCE public.user_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;
CREATE TABLE public."user"
(
    email text NOT NULL COLLATE pg_catalog."default",
    nome text NOT NULL COLLATE pg_catalog."default",
    password text NOT NULL COLLATE pg_catalog."default",
    ativo boolean,
    id integer NOT NULL DEFAULT nextval('user_id_seq'::regclass),
    "createdAt" timestamp with time zone,
    "updatedAt" timestamp with time zone,
    is_admin boolean DEFAULT false,
    CONSTRAINT user_pkey PRIMARY KEY (id),
    CONSTRAINT user_email_key UNIQUE (email),
     CONSTRAINT user_nome_key UNIQUE (nome)
)