
CREATE SEQUENCE public.db_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;
CREATE TABLE public.db
(
    versao text COLLATE pg_catalog."default",
    "appMin" text COLLATE pg_catalog."default",
    id integer NOT NULL DEFAULT nextval('db_id_seq'::regclass),
    "createdAt" timestamp with time zone,
    "updatedAt" timestamp with time zone,
    CONSTRAINT db_pkey PRIMARY KEY (id)
)