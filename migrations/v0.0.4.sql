CREATE SEQUENCE public.utfprservidores_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;
CREATE TABLE public."utfprservidor"
(
    nome text NOT NULL COLLATE pg_catalog."default",
    telefone integer NOT NULL,
    ramal integer,
	sala text COLLATE pg_catalog."default",
	cargo text NOT NULL COLLATE pg_catalog."default",
	matricula integer NOT NULL,
	celular integer NOT NULL,
	email text NOT NULL COLLATE pg_catalog."default",
    id integer NOT NULL DEFAULT nextval('utfprservidores_id_seq'::regclass),
    "createdAt" timestamp with time zone,
    "updatedAt" timestamp with time zone,
    CONSTRAINT utfprservidores_pkey PRIMARY KEY (id),
    CONSTRAINT utfprservidores_matricula_key UNIQUE (matricula),
	CONSTRAINT utfprservidores_celular_key UNIQUE (celular),
	CONSTRAINT utfprservidores_email_key UNIQUE (email)
)