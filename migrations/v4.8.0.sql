CREATE SEQUENCE public.ponto_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

CREATE TABLE public.ponto
(
    totalhoras real,
    id integer NOT NULL DEFAULT nextval('ponto_id_seq'::regclass),
    "createdAt" timestamp with time zone,
    "updatedAt" timestamp with time zone,
    hora_inicio real,
    hora_inicioalmoco real,
    hora_fimalmoco real,
    hora_fim real,
    id_func integer NOT NULL,
    CONSTRAINT banco_pkey PRIMARY KEY (id)
)