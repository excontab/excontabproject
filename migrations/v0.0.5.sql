CREATE SEQUENCE public.utfpralunos_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;
CREATE TABLE public."utfpraluno"
(
    nome text NOT NULL COLLATE pg_catalog."default",
	curso text NOT NULL COLLATE pg_catalog."default",
	ra integer NOT NULL,
	celular integer NOT NULL,
	email text NOT NULL COLLATE pg_catalog."default",
    id integer NOT NULL DEFAULT nextval('utfpralunos_id_seq'::regclass),
    "createdAt" timestamp with time zone,
    "updatedAt" timestamp with time zone,
    CONSTRAINT utfpralunos_pkey PRIMARY KEY (id),
    CONSTRAINT utfpralunos_ra_key UNIQUE (ra),
	CONSTRAINT utfpralunos_celular_key UNIQUE (celular),
	CONSTRAINT utfpralunos_email_key UNIQUE (email)
)