CREATE SEQUENCE public.utfprestacionamento_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;
CREATE TABLE public."utfprestacionamento"
(
    descricao text NOT NULL COLLATE pg_catalog."default",
	codigo text NOT NULL COLLATE pg_catalog."default",
    id integer NOT NULL DEFAULT nextval('utfprestacionamento_id_seq'::regclass),
    "createdAt" timestamp with time zone,
    "updatedAt" timestamp with time zone,
    CONSTRAINT utfprestacionamento_pkey PRIMARY KEY (id),
	CONSTRAINT utfprestacionamento_codigo_key UNIQUE (codigo)
)