CREATE SEQUENCE public.ocorrencias_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;
CREATE TABLE public."ocorrencias"
(
    id_veiculo integer NOT NULL,
	id_estacionamento integer NOT NULL,
	observacao text NOT NULL COLLATE pg_catalog."default",
    id integer NOT NULL DEFAULT nextval('ocorrencias_id_seq'::regclass),
    "createdAt" timestamp with time zone,
    "updatedAt" timestamp with time zone,
    CONSTRAINT ocorrencias_pkey PRIMARY KEY (id)
)